"""Solution for Shifty Bits"""
import sys
import math
import string


class Bitshifter:
    """Grouping of functions & datatypes for shifting bits"""
    def __init__(self):
        self.processing_map = {
            "int": self.__process_int,
            "uint": self.__process_uint,
            "float": self.__process_float,
            "double": self.__process_double
        }
        self.LOWER_FLOAT_BOUND: float = 0.00001
        self.UPPER_FLOAT_BOUND: float = 99999.99999
        self.FLOAT_EXP_BIAS: int = 127
        self.DOUBLE_EXP_BIAS: int = 1023

    def __process_int(self, message: str) -> string:
        """Convert binary to int"""
        if message[0] == "0":
            return f"{int(message, 2)}"
        # We know the number is negative and magnitude = 2 ** len(message) - 1
        message = message[1:]
        magnitude = len(message)
        return f"{-(2**magnitude) + int(message, 2)}"

    def __process_uint(self, message: str) -> string:
        """Convert binary to uint"""
        return f"{int(message, 2)}"

    @staticmethod
    def mantissa_to_decimal(mantissa: str) -> float:
        """Get everything behind the decimal point as a decimal value"""
        decimal_value: int = 0
        for index, num in enumerate(mantissa):
            decimal_value += int(num) * (1 / (2 ** (index + 1)))
        return 1 + decimal_value

    def better_round(self, num: float) -> str:
        """Assume 5 decimal places"""
        num_str = f"{round(num, 6)}"
        if int(num_str[-1]) == 5:
            num += 0.00001
        return round(num, 5)

    def __base_2_to_base_10(self, exponent: int) -> tuple[float, int]:
        """Base conversion returning mantissa and new exponent"""
        base_ten_exponent: float = exponent * math.log10(2)
        mantissa = 10 ** (base_ten_exponent - int(base_ten_exponent))
        return [mantissa, int(base_ten_exponent)]

    def __format_decimal(
        self, sign: int, exponent: int, decimal: float
    ) -> str:
        """
        Formats floats in scientific notation
        If not necessary, formats according to problem guidelines
        """
        converted_float = sign * (2 ** exponent) * decimal
        mantissa, exponent_b10 = self.__base_2_to_base_10(exponent)
        if (
            self.LOWER_FLOAT_BOUND < abs(converted_float) and
            abs(converted_float) < self.UPPER_FLOAT_BOUND
        ):
            return f"{self.better_round(converted_float)}"

        coeff = self.better_round((sign * decimal * mantissa))
        while abs(coeff) >= 10:
            coeff /= 10
            exponent_b10 += 1
        while abs(coeff) < 1:
            coeff *= 10
            exponent_b10 -= 1
        exponent_b10 = str(exponent_b10)
        while len(exponent_b10) < 3:
            exponent_b10 = f"0{exponent_b10}"

        if self.LOWER_FLOAT_BOUND > abs(converted_float):
            return f"{coeff}e{exponent_b10}"
        else:
            return f"{coeff}e+{exponent_b10}"

    def __process_float(self, message: str) -> string:
        """Convert binary to float, with sci notation if necessary"""
        sign: int = int(message[:1])
        exponent: str = message[1:9]
        mantissa: str = message[9:]
        sign = -((2 * sign) - 1)
        exponent = int(exponent, 2) - self.FLOAT_EXP_BIAS
        decimal = self.mantissa_to_decimal(mantissa)
        return self.__format_decimal(sign, exponent, decimal)

    def __process_double(self, message: str) -> string:
        """Convert binary to double, with sci notation if necessary"""
        sign: int = int(message[:1])
        exponent: str = message[1:12]
        mantissa: str = message[12:]
        sign = -((2 * sign) - 1)
        exponent = int(exponent, 2) - self.DOUBLE_EXP_BIAS
        decimal = self.mantissa_to_decimal(mantissa)
        return self.__format_decimal(sign, exponent, decimal)

    def __extract_measurands(
        self, binary_encoding: str, measurands: list[str]
    ) -> list[dict]:
        """Get measurands and datatypes from case"""
        binary_measurands: list[dict] = []

        for measurand in measurands:
            m_data: dict = {}
            m_data["datatype"], offset, m_length = measurand.split(' ')
            offset: int = int(offset)
            m_length: int = int(m_length)

            if offset == 0:
                m_data["value"] = binary_encoding[-(m_length):]
            else:
                m_data["value"] = binary_encoding[-(offset + m_length):-offset]
            binary_measurands.append(m_data)

        return binary_measurands

    def process_message(
        self, hex_data: str, measurands: list[str]
    ) -> None:
        """Handles individual message"""
        binary_encoding: str = f'{int(hex_data, 16):b}'
        message_data = self.__extract_measurands(binary_encoding, measurands)
        for message in message_data:
            print(self.processing_map[message["datatype"]](message["value"]))


def main():
    """Driver function"""
    bitshifter = Bitshifter()
    cases = int(sys.stdin.readline().rstrip())
    for _ in range(cases):
        hex_message = sys.stdin.readline().rstrip()
        measurand_count = sys.stdin.readline().rstrip()
        measurands = [
            sys.stdin.readline().rstrip() for i in range(int(measurand_count))
        ]
        bitshifter.process_message(hex_message, measurands)


if __name__ == "__main__":
    main()
